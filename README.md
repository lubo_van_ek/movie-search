# Setting api key

The default api key can be set in `.env` file under the key `REACT_APP_API_KEY`

## Things I wasn't trying to solve that would make this solution better (to save time)

- api not accesible or returning errors
- data loading - the experience could be quite jumpy at times
- using storybook for self-documenting components
- tests for utility functions
- data validation

## Styling

In order to save time a material UI library was chosen to handle styling and rensponsivity.
