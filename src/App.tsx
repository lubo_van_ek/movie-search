import { Pagination } from "@mui/material";
import React, { useEffect, useState } from "react";
import MovieReviews from "./components/MovieReviews";
import Movies from "./components/Movies";
import Search from "./components/Search";
import { MovieType } from "./types/MovieType";
import { TVShowType } from "./types/TVShowType";
import {
  buildGenresUrl,
  buildMoviesUrl,
  buildReviewsUrl,
  buildTrendingTVShowsUrl,
} from "./utilities/UrlBuilder";

const App: React.FC = () => {
  const [movies, setMovies] = useState([]);
  const [genres, setGenres] = useState([]);
  const [reviews, setReviews] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  const [page, setPage] = useState("1");
  const [totalReviewPages, setTotalReviewPages] = useState(1);
  const [reviewPage, setReviewPage] = useState("1");
  const [reviewOpen, setReviewOpen] = useState(false);
  const [selectedGenres, setSelectedGenres] = useState<string[]>([]);
  const [selectedYear, setSelectedYear] = useState<string>("");
  const [selectedMovie, setSelectedMovie] = useState<
    MovieType | TVShowType | null
  >(null);
  const [apiKey, setApiKey] = useState<string>("");

  useEffect(() => {
    async function fetchGenres(): Promise<void> {
      const res = await fetch(buildGenresUrl(apiKey));
      const data = await res.json();
      setGenres(data.genres);
    }
    fetchGenres();
  }, [apiKey]);

  useEffect(() => {
    async function fetchReviews(movieId: string): Promise<void> {
      const isMovie = selectedGenres.length !== 0 || selectedYear !== "";
      const res = await fetch(
        buildReviewsUrl(apiKey, isMovie, movieId, reviewPage)
      );
      const data = await res.json();
      setReviews(data.results);
      setTotalReviewPages(parseInt(data.total_pages, 10));
    }
    if (selectedMovie && reviewOpen) {
      fetchReviews(selectedMovie.id);
    } else {
      setReviews([]);
      setTotalReviewPages(1);
      setReviewPage("1");
    }
  }, [
    apiKey,
    reviewOpen,
    selectedMovie,
    selectedGenres,
    selectedYear,
    reviewPage,
  ]);

  useEffect(() => {
    async function fetchMovies(): Promise<void> {
      const isTVShow = selectedGenres.length === 0 && selectedYear === "";
      const url = isTVShow
        ? buildTrendingTVShowsUrl(apiKey, page)
        : buildMoviesUrl(apiKey, selectedGenres, selectedYear, page);
      const res = await fetch(url);
      const data = await res.json();
      setMovies(data.results);
      const pages = isTVShow
        ? parseInt(data.total_pages, 10)
        : Math.min(parseInt(data.total_pages, 10), 500);
      setTotalPages(pages);
    }
    fetchMovies();
  }, [apiKey, selectedGenres, selectedYear, page]);

  const handlePageChange = (
    _: React.ChangeEvent<unknown>,
    selectedPage: number
  ): void => {
    setPage(selectedPage.toString());
  };

  const handleReviewPageChange = (
    _: React.ChangeEvent<unknown>,
    selectedPage: number
  ): void => {
    setReviewPage(selectedPage.toString());
  };

  const getCurrentPage = (): number => {
    if (!Number.isNaN(page)) {
      return parseInt(page, 10);
    }
    return 1;
  };

  return (
    <div className="App">
      <Search
        genres={genres}
        setApiKey={setApiKey}
        setSelectedGenres={setSelectedGenres}
        setSelectedYear={setSelectedYear}
        resetPage={() => setPage("1")}
      />
      {totalPages > 1 && (
        <Pagination
          count={totalPages}
          onChange={handlePageChange}
          page={getCurrentPage()}
        />
      )}
      <Movies
        movies={movies}
        setSelectedMovie={setSelectedMovie}
        setReviewOpen={setReviewOpen}
      />
      <MovieReviews
        open={reviewOpen}
        setOpen={setReviewOpen}
        movie={selectedMovie}
        reviews={reviews}
        totalReviewPages={totalReviewPages}
        handlePageChange={handleReviewPageChange}
      />
    </div>
  );
};

export default App;
