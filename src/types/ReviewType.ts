export type ReviewType = {
  id: string;
  author: string;
  content: string;
};
