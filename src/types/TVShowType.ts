export type TVShowType = {
  id: string;
  name: string;
  backdrop_path: string;
  vote_average: string;
};
