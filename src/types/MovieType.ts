export type MovieType = {
  id: string;
  title: string;
  backdrop_path: string;
  vote_average: string;
};
