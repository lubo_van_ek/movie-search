const apikey = process.env.REACT_APP_API_KEY || "";

function getApiKey(userDefinedApiKey: string): string {
  return userDefinedApiKey !== "" ? userDefinedApiKey : apikey;
}

export const buildGenresUrl = (userDefinedApiKey: string): string => {
  const url = new URL("https://api.themoviedb.org/3/genre/movie/list");
  const params: Record<string, string> = {
    api_key: getApiKey(userDefinedApiKey),
  };
  url.search = new URLSearchParams(params).toString();
  return url.toString();
};

export const buildReviewsUrl = (
  userDefinedApiKey: string,
  isMovie: boolean,
  movieId: string,
  page: string
): string => {
  const url = new URL(
    `https://api.themoviedb.org/3/${
      isMovie ? "movie" : "tv"
    }/${movieId}/reviews`
  );
  const params: Record<string, string> = {
    api_key: getApiKey(userDefinedApiKey),
    page,
  };
  url.search = new URLSearchParams(params).toString();
  return url.toString();
};

export const buildTrendingTVShowsUrl = (
  userDefinedApiKey: string,
  page: string
): string => {
  const url = new URL("https://api.themoviedb.org/3/trending/tv/week");
  const params: Record<string, string> = {
    api_key: getApiKey(userDefinedApiKey),
    page,
  };
  url.search = new URLSearchParams(params).toString();
  return url.toString();
};

export const buildMoviesUrl = (
  userDefinedApiKey: string,
  selectedGenres: string[],
  selectedYear: string,
  page: string
): string => {
  const url = new URL("https://api.themoviedb.org/3/discover/movie");
  const params: Record<string, string> = {
    api_key: getApiKey(userDefinedApiKey),
    include_adult: "false",
    include_video: "false",
    language: "",
    page,
  };
  if (selectedGenres.length > 0) {
    params.with_genres = selectedGenres.toString();
  }
  if (selectedYear !== "" && !Number.isNaN(+selectedYear)) {
    params.primary_release_year = selectedYear;
  }
  url.search = new URLSearchParams(params).toString();
  return url.toString();
};

export const buildPosterUrl = (relativeImagePath: string): string =>
  `https://image.tmdb.org/t/p/w500${relativeImagePath}`;
