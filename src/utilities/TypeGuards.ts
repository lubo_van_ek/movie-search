import { TVShowType } from "../types/TVShowType";
import { MovieType } from "../types/MovieType";

export const determineIfIsMovieOrTVShowType = (
  toBeDetermined: MovieType | TVShowType
): toBeDetermined is MovieType => {
  if ((toBeDetermined as MovieType).title) {
    return true;
  }
  return false;
};
