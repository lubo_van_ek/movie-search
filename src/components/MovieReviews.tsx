import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import React, { Dispatch, SetStateAction } from "react";
import { IconButton, Pagination, Stack } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { MovieType } from "../types/MovieType";
import { ReviewType } from "../types/ReviewType";
import Review from "./Review";
import { TVShowType } from "../types/TVShowType";
import { determineIfIsMovieOrTVShowType } from "../utilities/TypeGuards";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "90%",
  height: "90%",
  bgcolor: "background.paper",
  overflow: "auto",
  boxShadow: 24,
  p: 3,
};
const MovieReviews: React.FC<{
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  movie: MovieType | TVShowType | null;
  reviews: ReviewType[];
  handlePageChange: (event: React.ChangeEvent<unknown>, page: number) => void;
  totalReviewPages: number;
}> = ({
  open,
  setOpen,
  movie,
  reviews,
  handlePageChange,
  totalReviewPages,
}) => {
  const handleClose = (): void => setOpen(false);
  let title = "";
  if (movie && determineIfIsMovieOrTVShowType(movie)) {
    title = movie.title;
  } else if (movie) {
    title = movie.name;
  }
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <IconButton
            aria-label="close"
            onClick={handleClose}
            sx={{
              position: "absolute",
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon />
          </IconButton>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Reviews ({title})
          </Typography>
          <Stack spacing={2}>
            {totalReviewPages > 1 ? (
              <Pagination
                count={totalReviewPages}
                onChange={handlePageChange}
              />
            ) : null}
            {reviews.length > 0 &&
              reviews.map((review) => (
                <Review key={review.id} review={review} />
              ))}
            {(reviews.length === 0 || Boolean(reviews) === false) && (
              <div>No reviews</div>
            )}
          </Stack>
        </Box>
      </Modal>
    </div>
  );
};

export default MovieReviews;
