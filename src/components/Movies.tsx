import { Grid } from "@mui/material";
import { Dispatch, SetStateAction } from "react";
import { MovieType } from "../types/MovieType";
import { TVShowType } from "../types/TVShowType";
import Movie from "./Movie";

const Movies: React.FC<{
  movies: MovieType[] | TVShowType[];
  setSelectedMovie: Dispatch<SetStateAction<MovieType | TVShowType | null>>;
  setReviewOpen: Dispatch<SetStateAction<boolean>>;
}> = ({ movies, setSelectedMovie, setReviewOpen }) => (
  <Grid container spacing={2} padding={2}>
    {movies &&
      movies.map((movie: MovieType | TVShowType) => (
        <Movie
          key={movie.id}
          movie={movie}
          setSelectedMovie={setSelectedMovie}
          setReviewOpen={setReviewOpen}
        />
      ))}
  </Grid>
);

export default Movies;
