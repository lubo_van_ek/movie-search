import { Button, Grid, TextField } from "@mui/material";
import React, { Dispatch, SetStateAction, useState } from "react";
import { MultipleSelect } from "react-select-material-ui";
import { GenreType } from "../types/GenreType";

type Option = { label: string; value: string };

const Search: React.FC<{
  genres: GenreType[];
  setSelectedGenres: Dispatch<SetStateAction<string[]>>;
  setSelectedYear: Dispatch<SetStateAction<string>>;
  setApiKey: Dispatch<SetStateAction<string>>;
  resetPage: () => void;
}> = ({ genres, setSelectedGenres, setSelectedYear, setApiKey, resetPage }) => {
  const [genreIds, setGenreIds] = useState<string[]>([]);
  const [year, setYear] = useState<string>("");
  const [userApiKey, setUserApiKey] = useState<string>("");

  const transformGenresToOptions: Option[] = genres
    ? genres.map((genre) => ({
        value: genre.id,
        label: genre.name,
      }))
    : [];

  const handleSearch = (event: React.FormEvent): void => {
    setSelectedGenres(genreIds);
    setSelectedYear(year);
    setApiKey(userApiKey);
    resetPage();
    event.preventDefault();
  };

  const handleGenreChange = (value: string[]): void => {
    setGenreIds(value);
  };

  const handleYearChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setYear(e.target.value);
  };

  const handleApiKeyChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setUserApiKey(e.target.value);
  };

  return (
    <form onSubmit={handleSearch}>
      <Grid container spacing={3} padding={3}>
        <Grid item xs={12} sm={6} md={3}>
          <TextField
            id="outlined-basic"
            label="ApiKey"
            variant="standard"
            fullWidth
            onChange={handleApiKeyChange}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <MultipleSelect
            label="Genre"
            variant="filled"
            onChange={handleGenreChange}
            options={transformGenresToOptions}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={2}>
          <TextField
            id="outlined-basic"
            label="Year"
            variant="standard"
            type="number"
            fullWidth
            onChange={handleYearChange}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <Button
            type="submit"
            variant="outlined"
            fullWidth
            style={{ height: "100%" }}
          >
            Search
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Search;
