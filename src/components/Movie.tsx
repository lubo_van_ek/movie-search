import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  Typography,
} from "@mui/material";
import { Dispatch, SetStateAction } from "react";
import { MovieType } from "../types/MovieType";
import { TVShowType } from "../types/TVShowType";
import { determineIfIsMovieOrTVShowType } from "../utilities/TypeGuards";
import { buildPosterUrl } from "../utilities/UrlBuilder";

const Movie: React.FC<{
  movie: MovieType | TVShowType;
  setSelectedMovie: Dispatch<SetStateAction<MovieType | TVShowType | null>>;
  setReviewOpen: Dispatch<SetStateAction<boolean>>;
}> = ({ movie, setSelectedMovie, setReviewOpen }) => {
  const handleOpen = (): void => {
    setSelectedMovie(movie);
    setReviewOpen(true);
  };
  const title = determineIfIsMovieOrTVShowType(movie)
    ? movie.title
    : movie.name;
  return (
    <Grid item xs={12} sm={6} md={4} lg={3}>
      <Card variant="outlined">
        <CardHeader title={title} titleTypographyProps={{ variant: "h6" }} />
        <CardMedia
          component="img"
          src={buildPosterUrl(movie.backdrop_path)}
          alt={`${title} poster`}
        />
        <CardContent>
          <Typography>Rating: {movie.vote_average}</Typography>
        </CardContent>
        <CardActions>
          <Button size="small" onClick={handleOpen}>
            View Reviews
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default Movie;
