import { Card, Typography } from "@mui/material";
import { ReviewType } from "../types/ReviewType";

const Review: React.FC<{ review: ReviewType }> = ({ review }) => (
  <Card variant="outlined" style={{ padding: "5px" }}>
    <Typography variant="h6">{review.author}</Typography>
    <Typography>{review.content}</Typography>
  </Card>
);

export default Review;
